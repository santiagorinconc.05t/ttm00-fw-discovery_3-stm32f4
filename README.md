# TTM00 - FW -Discovery_1 - STM32F4

This repo belongs to the series of internal FW training.

## Project Brief


## Prerequisites

* IDE + VERSION: STM32CubeIDE v1.3.0
* IDE + VERSION: STM32CubeMX  v5.6.1


## Versioning		


## Authors

The main application  was originally written by:

- **Santiago Rincon** santiago.rincon@titoma.com


## License



This project is a property of [TITOMA](https://titoma.com/) - All Rights Reserved
